// Home Automation System
//
// Base Class for a sensor module, that is common
// across all sensors
// Specific sensor types should inherit from this

#pragma once

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#include "Common.h"

//Generic data structure for recieving messages from server
struct RecieveMessage {
    //The command code can take the following values:
    //1 = Send sensor data
    //2 = Send battery level
    int commandCode;
};

//Generic data structure for sending data to the server
//Subclasses should cast this into their own custom struct
//Important: custom structs should not exceed sizeof(struct TransmitMessage)
struct TransmitMessage {
    char data[PAYLOAD_SIZE];
};

class Sensor {
    private:
        void printSetup();
        void networkSetup();
        bool recieve(struct RecieveMessage *data);
        void transmit(struct TransmitMessage *data);
    
    protected:
        RF24 radio;
        uint64_t readPipe;
        uint64_t writePipe;
    
        //Subclasses should override these methods
    
        //Perform basic sensor setup
        virtual void sensorSetup() = 0;
    
        //Retrieve sensor data and store it into 'data'
        virtual void getSensorData(struct TransmitMessage *data) = 0;
    
        //Return the name of the sensor
        virtual const char *sensorName() = 0;
    
    public:
        //Constructor
        Sensor(uint64_t readPipe, uint64_t writePipe) : radio(CE_PIN, CS_PIN), readPipe(readPipe), writePipe(writePipe) {}
    
        //Sensor setup code
        void setup();
    
        //To be called every iteration
        void loop();
};
