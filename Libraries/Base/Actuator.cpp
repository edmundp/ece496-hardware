// Home Automation System
//
// Base Class for an actuator module, that is common
// across all actuators
// Specific actuator types should inherit from this

#include "Actuator.h"
#include "printf.h"
#include "Encrypt.h"

void Actuator::printSetup() {
    Serial.begin(PRINT_BAUD_RATE);
    
    printf_begin();
    printf("Actuator Setup: %s\n", actuatorName());
}

void Actuator::networkSetup() {
    radio.begin();
    radio.setRetries(RETRY_DELAY, RETRY_COUNT);
    radio.setPayloadSize(PAYLOAD_SIZE);
    radio.setDataRate(DATA_RATE);
    radio.setChannel(CHANNEL);
    
    radio.openReadingPipe(1, readPipe);
    radio.openWritingPipe(writePipe);
    
    radio.startListening();
    radio.printDetails();
    
    printf("Network Setup: now listening...\n");
}

bool Actuator::recieve(struct RecieveMessage *data) {
    if (radio.available()) {
        
        bool done = false;
        while (!done)
        {
            done = radio.read(data, sizeof(struct RecieveMessage));
        }
        
        printf("\nREQ\n");
        
        return true;
    }
    
    return false;
}

void Actuator::transmit(struct TransmitMessage *data) {
    radio.stopListening();
    
    Serial.write("TX...");
    bool stat = radio.write(data, sizeof(struct TransmitMessage));
    
    if (stat)
        Serial.write("success\n");
    else
        Serial.write("failed\n");
    
    radio.startListening();
}

void Actuator::setup() {
    printSetup();
    networkSetup();
    actuatorSetup();
}

struct ActuatorRecieveMessage {
    char data[28];
    unsigned short command;
    unsigned short seqNum;
};

static unsigned short lastSeqNum = 0;

void Actuator::loop() {
    struct RecieveMessage recieveInfo;
    struct TransmitMessage transmitInfo;
    
    memset(&recieveInfo, 0, sizeof(struct RecieveMessage));
    memset(&transmitInfo, 0, sizeof(struct TransmitMessage));
    
    //See if it has recieved some data
    if (recieve(&recieveInfo)) {
        //Decrypt the data
        XORCipher((char *)(&recieveInfo), sizeof(struct RecieveMessage), (char *)(&readPipe), sizeof(readPipe));
        //printHex((char *)(&recieveInfo), sizeof(recieveInfo));
        
        //Check the sequence number
        struct ActuatorRecieveMessage *msg = (struct ActuatorRecieveMessage*)(&recieveInfo);
        
        unsigned short seqNum = msg->seqNum;
        
        //Make sure it's not a duplicate
        if (seqNum != lastSeqNum) {
            unsigned short command = msg->command;
            
            if (command == 1) {
                //Command = 1 means check if actuator is up
                //Simply return success
                memset(&transmitInfo, 0xFF, sizeof(struct TransmitMessage));
            }
            else {
                //Execute the command
                performAction(&recieveInfo, &transmitInfo);
                
                //Small delay to allow the server to switch to
                //listening mode
                delay(SERVER_SWITCH_DELAY);
            }
            
            lastSeqNum = seqNum;
        }
        else {
            printf("Got duplicate\n");
        }
        
        //Transmit the response
        transmit(&transmitInfo);
    }
}
