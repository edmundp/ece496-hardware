// Home Automation System
//
// Encryption functions


#include "Encrypt.h"

void XORCipher(char *string, int stringLen, char *key, int keyLen) {
    for (int x = 0; x < stringLen; x++)
    {
        string[x] = string[x]^key[x % keyLen];
    }
}

void printHex(char *string, int stringLen) {
    for (int i = 0; i < stringLen; i++) {
        printf("%X ", (int)string[i]);
    }
    
    printf("\n");
}
