// Home Automation System
//
// Base Class for a sensor module, that is common
// across all sensors
// Specific sensor types should inherit from this

#include "Sensor.h"
#include "printf.h"

void Sensor::printSetup() {
    Serial.begin(PRINT_BAUD_RATE);
    
    printf_begin();
    printf("Sensor Setup: %s\n", sensorName());
}

void Sensor::networkSetup() {
    radio.begin();
    radio.setRetries(RETRY_DELAY, RETRY_COUNT);
    radio.setPayloadSize(PAYLOAD_SIZE);
    radio.setAutoAck(1);
    radio.setDataRate(DATA_RATE);
    radio.setChannel(CHANNEL);
    radio.setPALevel(PA_LEVEL);
    radio.setCRCLength(CRC_LENGTH);
    
    radio.openReadingPipe(1, readPipe);
    radio.openWritingPipe(writePipe);
    
    radio.startListening();
    radio.printDetails();
    
    printf("Network Setup: now listening...\n");
}

bool Sensor::recieve(struct RecieveMessage *data) {
    if (radio.available()) {
        
        bool done = false;
        while (!done)
        {
            done = radio.read(data, sizeof(struct RecieveMessage));
        }
        
        printf("\nREQ\n");
        
        return true;
    }
    
    return false;
}

void Sensor::transmit(struct TransmitMessage *data) {
    radio.stopListening();
    
    Serial.write("TX...");
    bool stat = radio.write(data, sizeof(struct TransmitMessage));
    
    if (stat)
        Serial.write("success\n");
    else
        Serial.write("failed\n");
    
    radio.startListening();
}

void Sensor::setup() {
    printSetup();
    networkSetup();
    sensorSetup();
}

void Sensor::loop() {
    struct RecieveMessage recieveInfo;
    struct TransmitMessage transmitInfo;
    
    //See if it has recieved some data
    if (recieve(&recieveInfo)) {
        //Get sensor's data
        getSensorData(&transmitInfo);
        
        //Small delay to allow the server to switch to
        //listening mode
        delay(SERVER_SWITCH_DELAY);
        
        //Transmit sensor's data
        transmit(&transmitInfo);
    }
}
