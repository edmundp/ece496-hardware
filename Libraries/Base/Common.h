// Home Automation System
//
// Common defines are listed here

#pragma once

//SPI Pins for the nRF24L01+ chip
#define CE_PIN 9
#define CS_PIN 8

//Buffer size to transmit to and from server
#define PAYLOAD_SIZE 32

//Serial baud rate for print command (debugging)
#define PRINT_BAUD_RATE 57600

//Retry settings if connection error
#define RETRY_DELAY 15
#define RETRY_COUNT 15

//PA Level
#define PA_LEVEL RF24_PA_MAX

//CRC Length
#define CRC_LENGTH RF24_CRC_16

//Network speed
#define DATA_RATE RF24_250KBPS

//The channel to communicate on
#define CHANNEL 100

//Amount of time to allow the server to switch from
//transmitter mode to listening mode
#define SERVER_SWITCH_DELAY 20


