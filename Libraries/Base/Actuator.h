// Home Automation System
//
// Base Class for an actuator module, that is common
// across all actuators
// Specific actuator types should inherit from this

#pragma once

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#include "Common.h"

//Generic data structure for recieving messages (commands) from server
//Subclasses should cast the 'data' array into their own custom struct
//Important: custom structs should not exceed the size of the data array
struct RecieveMessage {
    char data[PAYLOAD_SIZE];
};

//Generic data structure for sending data back to the server
//Subclasses should cast the 'data' array into their own custom struct
//Important: custom structs should not exceed the size of the data array
struct TransmitMessage {
    unsigned short status;
    char data[30];
};

class Actuator {
    private:
        void printSetup();
        void networkSetup();
        bool recieve(struct RecieveMessage *data);
        void transmit(struct TransmitMessage *data);
    
    protected:
        RF24 radio;
        uint64_t readPipe;
        uint64_t writePipe;
    
        //Subclasses should override these methods
    
        //Perform basic actuator setup
        virtual void actuatorSetup() = 0;
    
        //Retrieve actuator data and store it into 'data'
        virtual void performAction(struct RecieveMessage *rx, struct TransmitMessage *tx) = 0;
    
        //Return the name of the actuator
        virtual const char *actuatorName() = 0;
    
    public:
        //Constructor
        Actuator(uint64_t readPipe, uint64_t writePipe) : radio(CE_PIN, CS_PIN), readPipe(readPipe), writePipe(writePipe) {}
    
        //Actuator setup code
        void setup();
    
        //To be called every iteration
        void loop();
};
