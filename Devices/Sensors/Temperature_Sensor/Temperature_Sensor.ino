#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <DHT.h>
#include <Sensor.h>

#include "TemperatureSensor.h"

#define READ_PIPE  0x8f240a0101LL
#define WRITE_PIPE 0x67fa663280LL

TemperatureSensor sensor(READ_PIPE, WRITE_PIPE);

void setup() {
  sensor.setup();
}

void loop() {
  sensor.loop();
}

