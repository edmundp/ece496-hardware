#include "TemperatureSensor.h"

//Perform basic sensor setup
void TemperatureSensor::sensorSetup() {
    dht.setup(DHT_PIN);
}

//Retrieve sensor data and store it into 'data'
void TemperatureSensor::getSensorData(struct TransmitMessage *data) {
    //delay(dht.getMinimumSamplingPeriod());
    
    float humidity = dht.getHumidity();
    float temperature = dht.getTemperature();
    
    struct TemperatureTransmitMessage *info = (struct TemperatureTransmitMessage *)data;
    
    info->success = (dht.getStatus() == DHT::ERROR_NONE);
    info->humidity = humidity;
    info->temperature = temperature;
    
    Serial.print(dht.getStatusString());
    Serial.print("\t");
    Serial.print(info->humidity);
    Serial.print("\t\t");
    Serial.print(info->temperature);
    Serial.print("\n");
}

//Return the name of the sensor
const char * TemperatureSensor::sensorName() {
    return "Temperature Sensor";
}
