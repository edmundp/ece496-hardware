// Home Automation System
//
// Temperature Sensor Class

#pragma once

#include <Sensor.h>
#include <DHT.h>
#include <Sensor.h>

#define DHT_PIN 2

struct TemperatureTransmitMessage {
    int success;
    int padding;
    float humidity;
    float temperature;
};

class TemperatureSensor : public Sensor {
    private:
        DHT dht;
    
    protected:
        virtual void sensorSetup();
        virtual void getSensorData(struct TransmitMessage * data);
        virtual const char *sensorName();

    public:
        TemperatureSensor(uint64_t readPipe, uint64_t writePipe) : Sensor(readPipe, writePipe) {}
};
