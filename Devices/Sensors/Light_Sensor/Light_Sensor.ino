#include <SPI.h>
#include <Wire.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <Sensor.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_TSL2561.h>
#include "LightSensor.h"

#define READ_PIPE  0x40948e92cdLL
#define WRITE_PIPE 0xcc13168aa5LL

LightSensor sensor(READ_PIPE, WRITE_PIPE);

void setup() {
  sensor.setup();
}

void loop() {
  sensor.loop();
}

