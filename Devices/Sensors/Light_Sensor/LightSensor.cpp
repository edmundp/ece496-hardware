#include "LightSensor.h"

//Perform basic sensor setup
void LightSensor::sensorSetup() {    
    if (!tsl.begin())
    {
        Serial.print("Ooops, no TSL2561 detected ... Check your wiring or I2C ADDR!");
        while(1);
    }
    
    sensor_t sensor;
    tsl.getSensor(&sensor);
    Serial.println("------------------------------------");
    Serial.print  ("Sensor:       "); Serial.println(sensor.name);
    Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
    Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
    Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" lux");
    Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" lux");
    Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" lux");
    Serial.println("------------------------------------");
    Serial.println("");
    
    tsl.enableAutoGain(true);
    tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_13MS);
}

//Retrieve sensor data and store it into 'data'
void LightSensor::getSensorData(struct TransmitMessage *data) {
    sensors_event_t event;
    tsl.getEvent(&event);
    
    if (event.light)
    {
        Serial.print(event.light); Serial.println(" lux");
    }
    else
    {
        Serial.println("Sensor overload");
    }
    
    struct LightTransmitMessage *info = (struct LightTransmitMessage *)data;
    
    info->success = (event.light != 0);
    info->lux = event.light;
}

//Return the name of the sensor
const char * LightSensor::sensorName() {
    return "Light Sensor";
}
