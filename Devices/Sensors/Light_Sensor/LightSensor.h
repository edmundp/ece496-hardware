// Home Automation System
//
// Light Sensor Class

#pragma once

#include <Sensor.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_TSL2561.h>

struct LightTransmitMessage {
    int success;
    int padding;
    float lux;
};

class LightSensor : public Sensor {
    private:
        Adafruit_TSL2561 tsl;
    
    protected:
        virtual void sensorSetup();
        virtual void getSensorData(struct TransmitMessage * data);
        virtual const char *sensorName();

    public:
        LightSensor(uint64_t readPipe, uint64_t writePipe) : Sensor(readPipe, writePipe), tsl(TSL2561_ADDR_FLOAT, 12345) {}
};
