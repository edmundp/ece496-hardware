// Home Automation System
//
// Motion Sensor Class

#pragma once

#include <Sensor.h>

#define SENSOR_PIN 2

struct MotionTransmitMessage {
    bool motionDetected;
};

class MotionSensor : public Sensor {
    protected:
        virtual void sensorSetup();
        virtual void getSensorData(struct TransmitMessage * data);
        virtual const char *sensorName();

    public:
        MotionSensor(uint64_t readPipe, uint64_t writePipe) : Sensor(readPipe, writePipe) {}
};
