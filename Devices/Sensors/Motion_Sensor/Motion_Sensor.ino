#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <Sensor.h>
#include "MotionSensor.h"

#define READ_PIPE  0x495b292523LL
#define WRITE_PIPE 0x8cbe3dd8f7LL

MotionSensor sensor(READ_PIPE, WRITE_PIPE);

void setup() {
  sensor.setup();
}

void loop() {
  sensor.loop();
}

