#include "MotionSensor.h"

//Perform basic sensor setup
void MotionSensor::sensorSetup() {
    pinMode(SENSOR_PIN, INPUT);
}

//Retrieve sensor data and store it into 'data'
void MotionSensor::getSensorData(struct TransmitMessage *data) {
    struct MotionTransmitMessage *info = (struct MotionTransmitMessage *)data;
    
    memset(info, 0,sizeof(info));
    info->motionDetected = digitalRead(SENSOR_PIN);

    Serial.print("\n");
    Serial.print(info->motionDetected);
    Serial.print("\n");
}

//Return the name of the sensor
const char * MotionSensor::sensorName() {
    return "Motion Sensor";
}
