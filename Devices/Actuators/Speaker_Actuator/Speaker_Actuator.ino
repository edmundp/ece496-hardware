#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <Actuator.h>
#include "SpeakerActuator.h"

#define READ_PIPE  0x8eefbc2303LL
#define WRITE_PIPE 0x2a198158b6LL

SpeakerActuator actuator(READ_PIPE, WRITE_PIPE);

void setup() {
  actuator.setup();
}

//code from http://arduino.cc/en/Tutorial/PlayMelody

// TONES  ==========================================
// Start by defining the relationship between 
//       note, period, &  frequency. 
#define  c1     3830    // 261 Hz 
#define  d1     3400    // 294 Hz 
#define  e1     3038    // 329 Hz 
#define  f1     2864    // 349 Hz 
#define  g1     2550    // 392 Hz 
#define  a1     2272    // 440 Hz 
#define  b1     2028    // 493 Hz 
#define  C1     1912    // 523 Hz 
// Define a special note, 'R', to represent a rest
#define  R1     0

int speakerOut = OUTPUT_PIN;

// MELODY and TIMING  =======================================
//  melody[] is an array of notes, accompanied by beats[], 
//  which sets each note's relative length (higher #, longer note) 
//int melody[] = {  
//  C1,  b1,  g1,  C1,  b1,   e1,  R1,  C1,  c1,  g1, a1, C1 };
int melody[] = {  
  c1, d1,e1,f1,g1,a1,b1,C1 };
int beats[]  = { 
  16, 16, 16,  8,  8,  16, 32, 16, 16, 16, 8, 8 }; 
int MAX_COUNT = sizeof(melody) / 2; // Melody length, for looping.

// Set overall tempo
long tempo = 10000;
// Set length of pause between notes
int pause = 1000;
// Loop variable to increase Rest length
int rest_count = 100; //<-BLETCHEROUS HACK; See NOTES

// Initialize core variables
int tone_ = 0;
int beat = 0;
long duration  = 0;

// PLAY TONE  ==============================================
// Pulse the speaker to play a tone for a particular duration
void playTone() {
  long elapsed_time = 0;
  if (tone_ > 0) { // if this isn't a Rest beat, while the tone has 
    //  played less long than 'duration', pulse speaker HIGH and LOW
    while (elapsed_time < duration) {

      digitalWrite(speakerOut,HIGH);
      delayMicroseconds(tone_ / 2);

      // DOWN
      digitalWrite(speakerOut, LOW);
      delayMicroseconds(tone_ / 2);

      // Keep track of how long we pulsed
      elapsed_time += (tone_);
    } 
  }
  else { // Rest beat; loop times delay
    for (int j = 0; j < rest_count; j++) { // See NOTE on rest_count
      delayMicroseconds(duration);  
    }                                
  }                                 
}

int i = 0;

void loop() {
  actuator.loop();

  //sound id not in use yet
  if (shouldPlaySound) {
    tone_ = melody[i];
    beat = beats[i];

    duration = beat * tempo; // Set up timing

    playTone(); 
    // A pause between notes...
    delayMicroseconds(pause);

    i++;
    i = i % MAX_COUNT;
  }
  else {
    i = 0;
  }
}



