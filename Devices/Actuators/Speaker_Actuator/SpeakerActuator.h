// Home Automation System
//
// Speaker Actuator Class

#pragma once

#include <Actuator.h>

#define OUTPUT_PIN 2

extern bool shouldPlaySound;
extern int soundID;

struct SpeakerActuatorRecieveMessage {
	int isOn;
        int soundID;
};

class SpeakerActuator : public Actuator {
    protected:
        virtual void actuatorSetup();
        virtual void performAction(struct RecieveMessage *rx, struct TransmitMessage *tx);
        virtual const char *actuatorName();

    public:
        SpeakerActuator(uint64_t readPipe, uint64_t writePipe) : Actuator(readPipe, writePipe) {}
};
