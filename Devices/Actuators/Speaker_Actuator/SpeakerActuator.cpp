#include "SpeakerActuator.h"

extern bool shouldPlaySound = false;
extern int soundID = 0;

void SpeakerActuator::actuatorSetup() {
    pinMode(OUTPUT_PIN, OUTPUT);
    digitalWrite(OUTPUT_PIN, LOW);
}

void SpeakerActuator::performAction(struct RecieveMessage *rx, struct TransmitMessage *tx) {
    struct SpeakerActuatorRecieveMessage *data = (struct SpeakerActuatorRecieveMessage *)rx;
	
    int isOn = data->isOn;
    soundID = data->soundID;
    
    printf("Is On: %d\n", (isOn == true));
    
    if (isOn == false) {
        shouldPlaySound = false;
    }
    else if (isOn == true) {
        shouldPlaySound = true;
    }
    
    tx->status = 1;
}

const char * SpeakerActuator::actuatorName() {
    return "Speaker Actuator";
}
