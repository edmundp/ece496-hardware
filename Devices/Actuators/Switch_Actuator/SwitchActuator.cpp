#include "SwitchActuator.h"

void SwitchActuator::actuatorSetup() {
    pinMode(OUTPUT_PIN, OUTPUT);
    digitalWrite(OUTPUT_PIN, LOW);
}

void SwitchActuator::performAction(struct RecieveMessage *rx, struct TransmitMessage *tx) {
    struct SwitchActuatorRecieveMessage *data = (struct SwitchActuatorRecieveMessage *)rx;
  
    int isOn = data->isOn;
    
    printf("Is On: %d\n", (isOn == true));
    
    if (isOn == false) {
        digitalWrite(OUTPUT_PIN, LOW);
    }
    else if (isOn == true) {
        digitalWrite(OUTPUT_PIN, HIGH);
    }
    
    tx->status = 1;
}

const char * SwitchActuator::actuatorName() {
    return "Switch Actuator";
}
