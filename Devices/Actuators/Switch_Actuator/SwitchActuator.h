// Home Automation System
//
// Switch Actuator Class

#pragma once

#include <Actuator.h>

#define OUTPUT_PIN 2

struct SwitchActuatorRecieveMessage {
  int isOn;
};

class SwitchActuator : public Actuator {
    protected:
        virtual void actuatorSetup();
        virtual void performAction(struct RecieveMessage *rx, struct TransmitMessage *tx);
        virtual const char *actuatorName();

    public:
        SwitchActuator(uint64_t readPipe, uint64_t writePipe) : Actuator(readPipe, writePipe) {}
};
