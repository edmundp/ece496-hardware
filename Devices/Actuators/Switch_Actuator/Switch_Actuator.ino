#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <Actuator.h>
#include "SwitchActuator.h"

#define READ_PIPE_S1  0x161f9bb9cdLL
#define WRITE_PIPE_S1 0xd6d169179bLL

#define READ_PIPE_S2  0x76ef93d9cdLL
#define WRITE_PIPE_S2 0x663162579bLL

#define READ_PIPE_S3  0x564f9879cdLL
#define WRITE_PIPE_S3 0x465166779bLL

#define READ_PIPE READ_PIPE_S2
#define WRITE_PIPE WRITE_PIPE_S2

SwitchActuator actuator(READ_PIPE, WRITE_PIPE);

void setup() {
  actuator.setup();
}

void loop() {
  actuator.loop();
}

